/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.helloclocktask1;

/**
 *
 * @author Farzana Akther
 * Date: 18 April, 2016
 * This class is intended to print the current date/time according to specified format
 */

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;


public class HelloClock {
  //Declaring custom date time format
  private final String CUSTOM_DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm:ss";
  
  /**
   * Constructor of HelloClock class with clock parameter
     * @param clock
   */
    public HelloClock(Clock clock) {
   
    //Initializing DateTimeFormatter with expected custom date time format
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(CUSTOM_DATE_TIME_FORMAT);
   
    //Fetching current date/time from constractor parameter clock and saving into local instant variable
    Instant now = clock.instant();
   
    //Printing custom format of current date time in console
    System.out.println("Current date/time with custom format from constructor: "+formatter.format(ZonedDateTime.ofInstant(now, ZoneId.systemDefault())));
   }

    public static void main(String[] args) {
 
    //Initializing clock with default time zone
    Clock clock = Clock.systemDefaultZone();
 
    //Calling the constructor with clock parameter
    HelloClock helloClock = new HelloClock(clock);
    }

    
}
